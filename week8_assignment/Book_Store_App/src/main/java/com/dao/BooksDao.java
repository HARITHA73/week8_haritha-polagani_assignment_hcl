package com.dao;

import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bean.Books;

@Repository
public class BooksDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	public List<Books> getAllBooks(){
		try {
			return jdbcTemplate.query("select * from books",new bookRowMapper());
		} catch (Exception e) {
			System.out.println("in retriving books"+e);
			return null;
		}
		
		
	}
	public int storeLikedBooks(Books books,String user) {
		try {
			return jdbcTemplate.update("insert into likedbooks values(?,?,?,?)",books.getBid(),books.getTitle(),
					books.getGenre(),books.getImageurl(),user);
		} catch (Exception e) {
			System.out.println("in liked books"+e);
			return 0;
		}
	}
	public int storeReadLaterBooks(Books books,String user) {
		try {
			return jdbcTemplate.update("insert into readlaterbooks values(?,?,?,?)",books.getBid(),books.getTitle(),
					books.getGenre(),books.getImageurl(),user);
		} catch (Exception e) {
			System.out.println("in Readlater books"+e);
			return 0;
		}
	}
	public List<Books> getAllLikedBooks(String user){
		try {
			return jdbcTemplate.query("select id,title,author,genre,imageurl  from likedbooks where user=?",new likedBookRowMapper(),user);
		} catch (Exception e) {
			System.out.println("in getlikedBooks"+e);
			return null;
		}

}
	public List<Books> getAllReadLaterBooks(String user){
		try {
			return jdbcTemplate.query("select id,title,author,genre,imageurl  from readlaterbooks where user=?",new readLaterBookRowMapper(),user);
		} catch (Exception e) {
			System.out.println("in getlikedBooks"+e);
			return null;
		}

}
}
class bookRowMapper implements RowMapper<Books>{

	@Override
	public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
		Books book=new Books();
		book.setBid(rs.getInt(1));
		book.setTitle(rs.getString(2));
		book.setGenre(rs.getString(3));
		book.setImageurl(rs.getString(4));
		return book;
	}
	
}
class likedBookRowMapper implements RowMapper<Books>{

	@Override
	public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
		Books lb=new Books();
		lb.setBid(rs.getInt(1));
		lb.setTitle(rs.getString(2));
		lb.setGenre(rs.getString(3));
		lb.setImageurl(rs.getString(4));
		return lb;
	}
	
}
class readLaterBookRowMapper implements RowMapper<Books>{

	@Override
	public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
		Books rl=new Books();
		rl.setBid(rs.getInt(1));
		rl.setTitle(rs.getString(2));
		rl.setGenre(rs.getString(3));
		rl.setImageurl(rs.getString(4));
		return rl;
	}
	
}


